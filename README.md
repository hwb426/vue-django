<div align="">
  <h1 align="">
    <span>电商小程序vue+Django</span>
  </h1>
<p align="">
    <a href="#">
        <img src="https://img.shields.io/badge/vue-2.x-green.svg" alt="vue Version">
    </a>
    <a href="#">
        <img src="https://img.shields.io/badge/Django->=3.0-green" alt="Django Version">
    </a>
      <a href="#">
        <img src="https://img.shields.io/badge/python-3.8+-green.svg" alt="python Version">
    </a>
      <a href="#">
        <img src="https://img.shields.io/badge/Mysql-5.6+-green.svg" alt="Mysql Version">
    </a>
    <a href="#">
        <img src="https://img.shields.io/badge/vant-2.x-green.svg" alt="vant Version">
    </a>
</p>



#### 项目简介
[vue-django](https://gitee.com/ly50/vue-django) 是一个完整的电商项目，前后端分离式开发，毫无保留给个人及企业免费使用。
工程化开发使用vuecli 3.0。
前端采用 [Vue](https://gitee.com/link?target=https%3A%2F%2Fcn.vuejs.org%2F)一套、[vant 2](https://vant-contrib.gitee.io/vant/v2/#/zh-CN/)、[axios](https://github.com/axios/axios)。
后端采用 Python 语言 Django 框架以及强大的 [Django REST Framework](https://gitee.com/link?target=https%3A%2F%2Fpypi.org%2Fproject%2Fdjangorestframework)。
权限认证使用[Django REST Framework SimpleJWT](https://gitee.com/link?target=https%3A%2F%2Fpypi.org%2Fproject%2Fdjangorestframework-simplejwt)，支持多终端认证系统。
支持加载动态权限菜单，多方式轻松权限控制。

**[演示地址 ](https://m.qyin.top/)**(下面有预览图)

[前端文档 ](https://gitee.com/ly50/vue-django)  **[后端文档 ](https://gitee.com/ly50/shop-django)**

**目录直达**

- [项目简介](#项目简介)
-  [项目结构](#项目结构) 
- [项目安装](#项目安装) 
- [修改配置](#修改配置) 
- [启动项目](#启动项目) 
- [实现功能](#实现功能) 
- [预览图示](#预览图示) 
- [最后想说](#最后想说) 

####  项目结构

```
vue-django\
  ├─ src  # 配置文件
  │  ├─api  # 异步请求模块
  │  ├─components  # 公共组件模块
  │  ├─router  # 路由模块
  │  ├─store  # 状态模块
  │  ├─views  # 视图模块
  │  ├─App.vue  # app入口
  │  └─main.js  # 配置模块
  ├─ public  # 静态资源文件
  ├─ docs  # 文档资源（可忽略）
  ├─ babel.config.js  # 配置vant
  ├─ package.json  # version配置
  └─ vue.config.js  # CLI全局配置
```

#### 项目安装

```bash
# 下载
git clone https://gitee.com/ly50/vue-django

# 安装依赖
npm install 

```

#### 修改配置

路径：vue-django\src\api\http.js


```javascript

import axios from 'axios'

// 设置默认api请求的地址
axios.defaults.baseURL = 'http://127.0.0.1:8050/goods-api/'

// 设置超时请求时间
axios.defaults.timeout = 10000

axios.defaults.headers['Content-Type'] = 'application/x-www-from-urlencoded'

// 设置请求拦截器
axios.interceptors.response.use(
  response => {
    // 返回响应主体
    return response.data
  },
  error => {
    return Promise.reject(error)
  }
)

export default axios

```

#### 启动项目


```bash
# 编译打包
npm run build
# 浏览器打开：vue-django\dist\index.html
```


####  实现功能

- 菜单管理：配置系统菜单，操作权限，按钮权限标识、后端接口权限等。

- 部门管理：配置系统组织机构（公司、部门、角色）。

- 角色管理：角色菜单权限分配、数据权限分配、设置角色按部门进行数据范围权限划分。

- 权限权限：授权角色的权限范围。

- 用户管理：用户是系统操作者，该功能主要完成系统用户配置。

- 接口白名单：配置不需要进行权限校验的接口。

- 字典管理：对系统中经常使用的一些较为固定的数据进行维护。

- 地区管理：对省市县区域进行管理。

- 附件管理：对平台上所有文件、图片等进行统一管理。

- 等可拓展更多功能......

 

 #### 预览图示


|  | |
|---------------------|:--------------------|
| ![](docs/assets/1.png) | ![](docs/assets/2.png) |
| ![](docs/assets/3.png) | ![](docs/assets/4.png) |
| ![](docs/assets/5.png) | ![](docs/assets/6.png) |
| ![](docs/assets/7.png) | ![](docs/assets/8.png) |



#### 最后想说

联系方式：

QQ：2486380377（邮箱同号）

WX：yin-only_

如果有机会希望我们可以一起聊聊技术生活唠嗑，一起学习进步。

希望看到的你生活愉快、工作顺利、解决所有bug！加油陌生人！