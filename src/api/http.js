import axios from 'axios'

// axios.defaults.baseURL = 'http://demoapi.qyin.top/goods-api/'
axios.defaults.baseURL = 'http://127.0.0.1:8050/goods-api/'

// 设置超时请求时间
axios.defaults.timeout = 10000

axios.defaults.headers['Content-Type'] = 'application/x-www-from-urlencoded'

// 设置请求拦截器
axios.interceptors.response.use(
  response => {
    // 返回响应主体
    return response.data
  },
  error => {
    return Promise.reject(error)
  }
)

export default axios
