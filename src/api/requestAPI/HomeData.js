import axios from '../http.js'

export default {
  // 首页Banner
  banner() {
    return axios.get('home/carousel/')
  },
  home_config() {
    return axios.get('home/config/')
  },
  goods_category(n = 0) {
    return axios.get('goods_category/' + n)
  },
  goods_getinfo(s) {
    return axios.get('goods/info/' + s)
  },
  goods_getinfoo(id) {
    return axios.get('goods/infoo/' + id)
  },
  goods_gethomeinfo(n1, n2 = 0) {
    return axios.get('home/info/' + n1 + '&' + n2)
  },
  user_login(l) {
    // (假 去登录)(真 去注册)
    if (l.nick) {
      return axios.post(`userzhuce/id=${l.id}&pwd=${l.pwd}&nick=${l.nick}`)
    } else {
      return axios.post(`userinfo/id=${l.id}&pwd=${l.pwd}`)
    }
  },
  goods_cart(goodsid, nick, num) {
    return axios.post(`cart/${goodsid}&${nick}&${num}`)
  },
  goods_getcart(nick) {
    return axios.get(`getcart/${nick}`)
  },
  goods_Cart(json) {
    return axios.put('goodscart/', json)
  },
  goods_Order(json, s) {
    if (s === 'post') {
      return axios.post('goodsorder/', json)
    }
    if (s === 'get') {
      return axios.get('goodsorder/' + json)
    }
  },
  getactget(s) {
    return axios.get(
      'https://cc.liyunfupay.cn/api.php?act=order&pid=1000&key=WBZHZWBeheKhWZcKuGRlb8lKWzwCWUeH&out_trade_no=' + s)
  }
}
