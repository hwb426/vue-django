import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import api from './api/index.js'

import {
  Tab,
  Tabs,
  Tabbar,
  TabbarItem,
  Icon,
  Lazyload,
  Swipe,
  SwipeItem,
  Grid,
  GridItem,
  Col,
  Row,
  Cell,
  CellGroup,
  Image as VanImage,
  Sidebar,
  SidebarItem,
  Card,
  GoodsAction,
  GoodsActionIcon,
  GoodsActionButton,
  Button,
  SubmitBar,
  Checkbox,
  CheckboxGroup,
  Sku,
  Toast,
  NavBar,
  List,
  Search,
  Sticky,
  Popup,
  ShareSheet,
  AddressList,
  ContactCard,
  RadioGroup,
  Radio,
  Form,
  Field,
  Stepper,
  Dialog
} from 'vant'
Vue.use(Dialog).use(Stepper).use(Form).use(Field).use(Radio).use(RadioGroup).use(AddressList).use(ContactCard).use(ShareSheet).use(Popup).use(Sticky).use(Search).use(List).use(NavBar).use(Toast).use(Sku)
  .use(CheckboxGroup).use(Checkbox).use(SubmitBar)
  .use(Button).use(GoodsAction).use(GoodsActionButton).use(GoodsActionIcon).use(Card).use(Tabs).use(Tab).use(
    SidebarItem).use(
    Sidebar).use(VanImage).use(Cell).use(CellGroup).use(Row).use(Col)
  .use(Icon).use(Tabbar).use(
    TabbarItem).use(Lazyload).use(
    Swipe).use(
    SwipeItem).use(Grid).use(
    GridItem)

Vue.config.productionTip = false

Vue.prototype.$api = api

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
