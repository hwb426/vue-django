import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'
import Category from '../views/Category'
import Cart from '../views/Cart'
import Me from '../views/Me'
import Details from '../views/Details'
import Order from '../views/Order'
import Login from '../views/Login'
import Payment from '../views/Payment'

Vue.use(VueRouter)

const routes = [{
    path: '*',
    redirect: 'Home'
  },
  {
    path: '/',
    redirect: 'Home'
  },
  {
    path: '/Home',
    name: 'Home',
    component: Home
  },
  {
    path: '/Category',
    name: 'Category',
    component: Category
  },
  {
    path: '/Cart',
    name: 'Cart',
    component: Cart
  },
  {
    path: '/Me',
    name: 'Me',
    component: Me
  },
  {
    path: '/Login',
    name: 'Login',
    component: Login
  },
  {
    path: '/Details/:id',
    name: 'Details',
    component: Details,
    props: true
  },
  {
    path: '/Order',
    name: 'Order',
    component: Order
  },
  {
    path: '/Payment',
    name: 'Payment',
    component: Payment
  }
]

const router = new VueRouter({
  routes
})

export default router
