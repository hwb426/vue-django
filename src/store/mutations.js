import {
  SET_BANNER_IMAGES,
  SET_HOME_CONFIG,
  SET_GOODS_CATEGORY
} from './types.js'

export default {
  [SET_BANNER_IMAGES](state, item) {
    state.bannerImages = item
    localStorage.setItem('bannerImages', JSON.stringify(item))
  },
  [SET_HOME_CONFIG](state, item) {
    state.homeconfig = item
    localStorage.setItem('homeconfig', JSON.stringify(item))
  },
  SET_HOME_HOT(state, item) {
    state.homehot = item
    localStorage.setItem('homehot', JSON.stringify(item))
  },
  SET_HOME_NPD(state, item) {
    state.homenpd = item
    localStorage.setItem('homenpd', JSON.stringify(item))
  },
  SET_HOME_RMD(state, item) {
    state.homermd = item
    localStorage.setItem('homermd', JSON.stringify(item))
  },
  [SET_GOODS_CATEGORY](state, item) {
    state.goods_category = item
    localStorage.setItem('goods_category', JSON.stringify(item))
  },
  SET_GOODS_INFO(state, item) {
    state.goods_info = item
    localStorage.setItem('goods_info', JSON.stringify(item))
  },
  SET_RMD_GOODS_GETINFO(state, item) {
    state.goods_getinfo_one = item
    localStorage.setItem('goods_getinfo_one', JSON.stringify(item))
  },
  SET_GOODSCART(state, item) {
    state.goodscarts = item
    localStorage.setItem('goodscarts', JSON.stringify(item))
  },
  SET_GOODS_ORDER(state, item) {
    state.goods_order = item
    localStorage.setItem('goods_order', JSON.stringify(item))
  },
  SET_GOODS_ORDER_ME(state, item) {
    state.goods_order_me = item
    localStorage.setItem('goods_order_me', JSON.stringify(item))
  }
}
