export default {
  bannerImages: JSON.parse(localStorage.getItem('bannerImages')) || [],
  homeconfig: JSON.parse(localStorage.getItem('homeconfig')) || [],
  goods_category: JSON.parse(localStorage.getItem('goods_category')) || [],
  goods_info: JSON.parse(localStorage.getItem('goods_info')) || [],
  goods_getinfo_one: JSON.parse(localStorage.getItem('goods_getinfo_one')) || [],
  homehot: JSON.parse(localStorage.getItem('homehot')) || [],
  homenpd: JSON.parse(localStorage.getItem('homenpd')) || [],
  homermd: JSON.parse(localStorage.getItem('homermd')) || [],
  goodscarts: JSON.parse(localStorage.getItem('goodscarts')) || [],
  goods_order: JSON.parse(localStorage.getItem('goods_order')) || [],
  goods_order_me: JSON.parse(localStorage.getItem('goods_order_me')) || []
}
