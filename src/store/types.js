export const SET_BANNER_IMAGES = 'SET_BANNER_IMAGES' // 设置banner图片
export const SET_HOME_CONFIG = 'SET_HOME_CONFIG' // 设置 Home的配置数据
export const SET_GOODS_CATEGORY = 'SET_GOODS_CATEGORY' // 设置 Goods 图片

export default {
  SET_BANNER_IMAGES,
  SET_HOME_CONFIG,
  SET_GOODS_CATEGORY
}
